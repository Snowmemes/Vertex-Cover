#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;

struct Node{
	bool 	hasParent 	= false;
	Node  	**children 	= NULL;
	size_t 	nChildren 	= 0;
	size_t 	maxChildren = 5;
	int 	value		= 0;
};

Node* createNode () {
	Node * result = new Node;
	result->children = new Node*[result->maxChildren]; 
	return result;
}

void addToChildrenField (Node* lukeIAMYourfather, Node* NOOOOOOOOO){
	if ( lukeIAMYourfather->nChildren >= lukeIAMYourfather->maxChildren ){
		lukeIAMYourfather->maxChildren = 1.5*lukeIAMYourfather->maxChildren;
		Node ** newField = new Node*[lukeIAMYourfather->maxChildren];
		copy (	lukeIAMYourfather->children,
				lukeIAMYourfather->children + lukeIAMYourfather->nChildren,
				newField );
		delete [] lukeIAMYourfather->children;
		lukeIAMYourfather->children = newField;
	}
	lukeIAMYourfather->children[(lukeIAMYourfather->nChildren)++] = NOOOOOOOOO;
}

void addChildren (Node* lukeIAMYourfather, Node*  NOOOOOOOOO) {
	//Node * NOOOOOOOOO = createNode();
	addToChildrenField (lukeIAMYourfather, NOOOOOOOOO);
	NOOOOOOOOO->hasParent = true;
}

// void static printTree (Node* root) {
	// 	if (root)
	// 		cout << " root - " << root->nChildren <<" children: " << endl;
	// 	else {
	// 		cout << "nope";
	// 		return;
	// 	}
	// 	Node * tmp = root;
	// 	queue <Node*> zasobnik;
	// 	while ( 1 ){
	// 		for (int i = 0; i < tmp -> nChildren; ++i){
	// 			cout << " " << i << " ";
	// 			zasobnik.push(tmp->children[i]);
	// 		}
	// 		if(!zasobnik.empty()){
	// 			tmp = zasobnik.front();
	// 			zasobnik.pop();
	// 			if ( tmp -> nChildren != 0 )
	// 				cout << endl;
	// 		}

	// 		else{
	// 			cout << endl;
	// 			return;
	// 		}
	// 	}

	//}


int vertexCalculator (Node*  root) {
	 if ( root->nChildren == 0 || root == NULL ) //skip all the calculations
		return 0;
	
	if ( root->value != 0 ) //let us not count again
		return root->value;

	int sizeVertexIncluded = 1; //im counting in that vertex is in the cover
	for ( int i = 0; i < root->nChildren ; ++i)
		sizeVertexIncluded += vertexCalculator(root->children[i]); //add vertex cover result of the whole subtree

	int sizeVertexExcluded = root->nChildren; // vertex is not covered, so then its children have to be
	for ( int i = 0; i < root->nChildren ; ++i)
		for (int j = 0; j < root->children[i]->nChildren; ++j)
			sizeVertexExcluded += vertexCalculator(root->children[i]->children[j]); //but not their children, so we have to
																					//go deeper

	root->value =	sizeVertexIncluded < sizeVertexExcluded ? 
					sizeVertexIncluded : 
					sizeVertexExcluded ;  //surprise surprise, we want the minimum

	return root->value;
}

int startVertex (Node*  root){ //nejak to bugovalo pripad pouze pro root
	// Node * devka = createNode ();
	// addChildren ( devka, root );
	// if ( root -> nChildren == 0 )
	// 	return 0;

	return vertexCalculator ( root );
}

int main () {
	size_t numberOfNodes;
	cin >> numberOfNodes;

	Node ** allNodes = new Node* [numberOfNodes];

	for (size_t i = 0; i < numberOfNodes; ++i)
		allNodes[i] = createNode ();

	int u, v;
	Node * root = allNodes[0];

	for (size_t i = 0; i < numberOfNodes-1; ++i){
		cin >> u >> v;

		if ( u > v ){  //u je mensi
			int tmp = u;
			u = v;
			v = tmp;
		}

		addChildren(allNodes[u-1], allNodes[v-1]);
	}

	//nvm jestli je prvni node root nebo ne

	for (size_t i = 0; i < numberOfNodes; i++) {
		if ( !allNodes[i]->hasParent ) {
			root = allNodes[i];
			break;
		}
	}

	cout << startVertex(root) << endl ;
	return 0;
}
