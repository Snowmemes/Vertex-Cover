#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;
int ahoj = 0, cau = 0;;
struct Node{
	// bool 	hasParent 	= false;
	Node  	**Neighbours 	= NULL;
	size_t 	nNeighbours 	= 0;
	size_t 	maxNeighbours 	= 5;
	int 	value			= 0;
	int index = ahoj++;
};

Node* createNode () {
	Node * result = new Node;
	result -> Neighbours = new Node*[result->maxNeighbours]; 
	return result;
}

void addToNeighboursField (Node* father , Node* son){
	if ( father -> nNeighbours >= father -> maxNeighbours ){
		father ->maxNeighbours = 2*father -> maxNeighbours;
		Node ** newField = new Node*[ father -> maxNeighbours];
		// copy (	father ->Neighbours,
		// 		father ->Neighbours + father ->nNeighbours,
		// 		newField );
		for ( int i = 0; i < father-> nNeighbours; ++i){
			newField[i] = father->Neighbours[i];
		}
		delete [] father ->Neighbours;
		father -> Neighbours = newField;
	}
	father -> Neighbours[(father -> nNeighbours)++] = son;
}

void addNeighbours (Node* father , Node*  son) {
	//Node * son = createNode();
	addToNeighboursField (father , son);
	cau++;
	// son->hasParent = true;
}



int vertexCalculator (Node*  root, Node * padre) {
	 if ( root->nNeighbours == 1 ){ //skip all the calculations
	 	cout << " demente " << endl;
		return 0;
	}

	if ( root->value != 0 ){ //let us not count again
		cout << " counted " << endl;
		return root->value;
	}

	int sizeVertexIncluded = 1; //im counting in that vertex is in the cover
	for ( int i = 0; i < root->nNeighbours ; ++i){
		if ( root->Neighbours[i] != padre)
		sizeVertexIncluded += vertexCalculator(root->Neighbours[i], root); //add vertex cover result of the whole subtree
	}
	cout << " mezi ";
	int sizeVertexExcluded = root->nNeighbours - 1; // vertex is not covered, so then its Neighbours have to be
	for ( int i = 0; i < root->nNeighbours ; ++i){
		if ( root->Neighbours[i] == padre){
			continue;
		}
		for (int j = 0; j < root->Neighbours[i]->nNeighbours; ++j){
			if ( root->Neighbours[i] != root->Neighbours[i]->Neighbours[j] ){
				// cout << "lul" << endl;
				sizeVertexExcluded += vertexCalculator(root->Neighbours[i]->Neighbours[j], root); //but not their Neighbours, so we have to
										//go deeper
			}
		}
	}
	cout << sizeVertexIncluded << " < ? > " << sizeVertexExcluded << endl;
	root->value =	sizeVertexIncluded < sizeVertexExcluded ? 
					sizeVertexIncluded : 
					sizeVertexExcluded ;  //surprise surprise, we want the minimum

	return root->value;
}

int startVertex (Node*  root){ //nejak to bugovalo pripad pouze pro root
	return vertexCalculator ( root, NULL );
}

int main () {
	size_t numberOfNodes;
	cin >> numberOfNodes;
	Node ** allNodes = new Node* [numberOfNodes];

	for (size_t i = 0; i < numberOfNodes; ++i)
		allNodes[i] = createNode ();

	int u, v;

	Node * root = allNodes[0];

	for (size_t i = 0; i < numberOfNodes-1; ++i){
		cin >> u >> v;
		// if ( u > v ){  //u je mensi
		// 	int tmp = u;
		// 	u = v;
		// 	v = tmp;
		// }
		addNeighbours(allNodes[v-1], allNodes[u-1]);
		addNeighbours(allNodes[u-1], allNodes[v-1]);

	}
	cout << cau << endl;
	for ( int i = 0; i < numberOfNodes-1; ++i){
		if ( allNodes[i]->nNeighbours > 2 ){
			root = allNodes[i];
			break;
		} 
	}
	//nvm jestli je prvni node root nebo ne

	// for (size_t i = 0; i < numberOfNodes; i++) {
	// 	if ( !allNodes[i]->hasParent ) {
	// 		root = allNodes[i];
	// 		break;
	// 	}
	// }
	// printTree( root );
	cout << startVertex(root) << endl ;
	cout << numberOfNodes;
	return 0;
}
