#include <iostream>

using namespace std;
struct Node {
	int nNeighbours;
	int maxNeighbours;
	int value;
	Node** neighbours;
};
void connectNodes ( Node* one, Node* two ){
	if ( one->nNeighbours >= one->maxNeighbours ){
		one->maxNeighbours *= 1.5;
		Node** tmp = new Node*[one->maxNeighbours];
		for ( int i = 0; i < one->nNeighbours; ++i){
			tmp[i] = one->neighbours[i];
		}
		delete [] one->neighbours;
		one->neighbours = tmp;
	}
	one->neighbours[(one->nNeighbours)++] = two;
	cout << one->nNeighbours; 
	if ( two->nNeighbours >= two->maxNeighbours ){
		two->maxNeighbours *= 1.5;
		Node** tmp = new Node*[two->maxNeighbours];
		for ( int i = 0; i < two->nNeighbours; ++i){
			tmp[i] = two->neighbours[i];
		}
		delete [] two->neighbours;
		two->neighbours = tmp;
	}
	two->neighbours[(two->nNeighbours)++ ] = one;
	cout << two->nNeighbours << endl; 
}

Node* doMeANode (){
	Node* tmp = new Node;
	tmp->nNeighbours = 0;
	tmp->maxNeighbours = 8;
	tmp->value = 0;
	tmp->neighbours = new Node*[tmp->nNeighbours];
	return tmp;
}

int main (){
	int numberOfNodes, u, v;
	cin >> numberOfNodes;
	int maxN = 0, index = 0;

	Node** allNodes = new Node*[numberOfNodes];
	for ( int i = 0; i < numberOfNodes; ++i)
		allNodes[i] = doMeANode();
	for (int i = 0; i < numberOfNodes - 1; i ++){
		cin >> u >> v;
		connectNodes(allNodes[u-1], allNodes[v-1]);

		// if ( allNodes[u-1]->nNeighbours > maxN ){
		// 	maxN = allNodes[u-1]->nNeighbours;
		// 	index = u-1;
		// }
		// if ( allNodes[v-1]->nNeighbours > maxN ){
		// 	maxN = allNodes[u-1]->nNeighbours;
		// 	index = v-1;
		// }
	}
	for ( int i = 0; i < numberOfNodes; ++i){
		cout << allNodes[i]->nNeighbours << endl;
	}
	// cout << vertexCover( root );

}