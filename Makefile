
CXX=g++
CXX_FLAGS=-pedantic -Wall -Wextra -std=c++11 -fsanitize=address

.PHONY: test

vertexCoverGraph2.out:
	$(CXX) $(CXX_FLAGS) vertexCoverGraph2.cpp -o vertexCoverGraph2.out

test: vertexCoverGraph2.out
	lua ./randomise.lua < ./unRandomizedProblematicInput | ./vertexCoverGraph2.out

clean:
	rm -f ./*.out ./.incr

