#!/bin/luajit

local seed = 0
local f = io.open( ".incr", "r" )
if f then
	seed = f:read "*n" or 0
	f:close()
end

math.randomseed( seed * 1000 )

for i = 1, seed do
	math.random()
end

local n = io.read "*n"
print( n )

local results = {}
for i = 1, n - 1 do
	local a = io.read "*n"
	local b = io.read "*n"

	results[ #results + 1 ] = { a, b }
end

for i = 1, n - 1 do
	local r = table.remove( results, math.random( 1, #results ) )
	local a, b = r[ 1 ], r[ 2 ]

	if math.random() > 0.5 then
		print( a, b )
	else
		print( b, a )
	end
end

f = io.open( ".incr", "w" )
if f then
	f:write( seed + 1 )
	f:write( "\n" )
	f:close()
end

